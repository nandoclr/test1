import 'package:flutter/material.dart';
import 'package:tienda/src/widgets/drawerMenu.widget.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Inicio'),
        ),
        drawer: MenuWidget(),
        body: Center(child: Text('data')));
  }
}
