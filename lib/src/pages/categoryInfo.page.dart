import 'package:flutter/material.dart';
import 'package:tienda/src/widgets/alertCallback.widget.dart';
import 'package:tienda/src/widgets/alertInfo.widget.dart';
import 'package:tienda/src/widgets/alertEditCat.widget.dart';

// import '../services/http.category.service.dart' as http;
import '../services/http.category.service.dart';
import '../models/category.model.dart';

class CategoryInfoPage extends StatelessWidget {
  CategoryItem catItem = new CategoryItem();
  @override
  Widget build(BuildContext context) {
    final args =
        ModalRoute.of(context).settings.arguments as Map<String, String>;
    return Container(
        child: Scaffold(
      appBar: AppBar(
        title: Text('Categoria'),
        leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.pop(context)),
        actions: [
          IconButton(
              icon: Icon(Icons.delete),
              onPressed: () async {
                AlertCallback().showAlertDialog(context,
                    title: 'Atencion.',
                    content: 'Se eliminara esta categoria. Desea continuar?',
                    callBack: () async {
                  Navigator.pop(context);
                  bool err = await HttpCategoryService().removeCat(args['id']);
                  if (!err) return Navigator.pushNamed(context, 'categorias');
                  AlertInfo().showAlertDialog(context,
                      title: 'Error!!', content: 'Error borrando categoria.');
                });
              }),
          IconButton(
              icon: Icon(Icons.edit),
              onPressed: () =>
                  EditCategoryAlert().editCategory(context, catItem, 'Editar'))
        ],
      ),
      body: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.all(20),
        child: FutureBuilder(
            future: HttpCategoryService().getSpecificCat(args['id']),
            builder: (context, AsyncSnapshot<CategoryItem> snapshot) {
              if (snapshot.hasData) {
                catItem = snapshot.data;
                return Column(
                  children: [
                    Text('Nombre de categoria: ' + snapshot.data.name),
                    (snapshot.data.parentId == null)
                        ? Text('Categoria padre: Ninguna')
                        : FutureBuilder(
                            future: HttpCategoryService().getSpecificCat(
                                snapshot.data.parentId.toString()),
                            builder: (context,
                                AsyncSnapshot<CategoryItem> snapshot) {
                              if (snapshot.hasData) {
                                return Text(
                                    'Categoria padre: ' + snapshot.data.name);
                              } else
                                return CircularProgressIndicator();
                            }),
                    Text(snapshot.data.description == ''
                        ? 'Descripcion: Ninguna'
                        : 'Descripcion: ' + snapshot.data.description),
                    Text('Fecha de creacion: ' +
                        snapshot.data.creationDate.toString()),
                    Text(snapshot.data.lastModificationDate == null
                        ? 'Ultima modificacion: Nunca'
                        : 'Ultima modificacion: ' +
                            snapshot.data.lastModificationDate.toString()),
                  ],
                );
              } else
                return Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(),
                      Container(
                          margin: EdgeInsets.only(left: 25),
                          child: Text('Cargando...')),
                    ],
                  ),
                );
            }),
      ),
    ));
  }
}
