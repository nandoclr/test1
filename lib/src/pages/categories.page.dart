import 'package:flutter/material.dart';
import 'package:tienda/src/widgets/alertEditCat.widget.dart';

import 'package:tienda/src/widgets/drawerMenu.widget.dart';
import 'package:tienda/src/widgets/menuItemList.widget.dart';
import '../models/category.model.dart';

import '../services/http.category.service.dart';

class CategoriasPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Categorias'),
        ),
        drawer: MenuWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            CategoryItem newCat = new CategoryItem();
            EditCategoryAlert().editCategory(context, newCat, 'Crear');
          },
        ),
        body: Container(
          padding: EdgeInsets.all(5),
          child: FutureBuilder(
            future: HttpCategoryService().getAllCats('3'), //Tienda con id = 3
            builder: (context, AsyncSnapshot<List<CategoryItem>> snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, i) {
                      return GestureDetector(
                        child: MenuItemList(
                          item: snapshot.data[i],
                        ),
                        onTap: () {
                          Navigator.pushNamed(context, 'categoryInfo',
                              arguments: {
                                'id': snapshot.data[i].categoryId.toString()
                              });
                        },
                      );
                    });
              } else
                return Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(),
                      Container(
                          margin: EdgeInsets.only(left: 25),
                          child: Text('Cargando...')),
                    ],
                  ),
                );
            },
          ),
        ));
  }
}
