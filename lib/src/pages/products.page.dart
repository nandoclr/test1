import 'package:flutter/material.dart';
import 'package:tienda/src/widgets/drawerMenu.widget.dart';

class ProductosPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Productos'),
      ),
      drawer: MenuWidget(),
      body: Center(
        child: Text('Productos'),
      ),
    );
  }
}
