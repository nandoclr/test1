import 'package:flutter/material.dart';

//Drawer menu items
List<Map<String, dynamic>> menuItems = [
  {'name': 'Inicio', 'icon': Icons.home, 'route': 'home'},
  {'name': 'Categorias', 'icon': Icons.category, 'route': 'categorias'},
  {'name': 'Productos', 'icon': Icons.today_outlined, 'route': 'productos'},
  {'name': 'Punto de venta', 'icon': Icons.album_outlined, 'route': 'venta'},
  {'name': 'Login', 'icon': Icons.login, 'route': 'login'}
];

//Drawer logo
String iconLogo = 'assets/img/logo.png';
String textLogo = 'Bettle CMS';
