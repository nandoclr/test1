class CategoryItem {
  CategoryItem({
    this.categoryId,
    this.name,
    this.description,
    this.parentId,
    this.storeId,
    this.lastModificationDate,
    this.creationDate,
  });

  int categoryId;
  String name;
  String description;
  int parentId;
  int storeId;
  dynamic lastModificationDate;
  DateTime creationDate;

  factory CategoryItem.fromJson(Map<String, dynamic> json) => CategoryItem(
        categoryId: json["category_id"],
        name: json["name"],
        description: json["description"],
        parentId: json["parent_id"],
        storeId: json["store_id"],
        lastModificationDate: json["last_modification_date"] == null
            ? null
            : DateTime.parse(json["last_modification_date"]),
        creationDate: DateTime.parse(json["creation_date"]),
      );

  Map<String, dynamic> toJson() => {
        "category_id": categoryId,
        "name": name,
        "description": description,
        "parent_id": parentId,
        "store_id": storeId,
        "last_modification_date": lastModificationDate.toIso8601String(),
        "creation_date": creationDate.toIso8601String(),
      };
}
