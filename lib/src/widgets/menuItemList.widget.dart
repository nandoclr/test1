import 'package:flutter/material.dart';

import '../models/category.model.dart';

class MenuItemList extends StatelessWidget {
  final CategoryItem item;

  const MenuItemList({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        margin: EdgeInsets.all(5),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey[400],
              offset: Offset(1.0, 1.0),
              blurRadius: 2.0,
            ),
          ],
          borderRadius: BorderRadius.circular(3),
          color: Colors.white,
        ),
        child: ListTile(
          title: Text(item.name),
          trailing: Icon(Icons.arrow_forward_ios),
        ),
      ),
    );
  }
}
