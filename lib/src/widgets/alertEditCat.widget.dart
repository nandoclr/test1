import 'package:flutter/material.dart';
import 'package:tienda/src/models/category.model.dart';

import '../services/http.category.service.dart';

//Falta
//Traer las categorias para listarlas

class EditCategoryAlert {
  editCategory(BuildContext context, CategoryItem categoryItem, String action) {
    final _editForm = GlobalKey<FormState>();
    TextEditingController _nombreCtrl = new TextEditingController();
    TextEditingController _descriptionCtrl = new TextEditingController();
    _nombreCtrl.text = categoryItem.name;
    _descriptionCtrl.text = categoryItem.description;

    AlertDialog alert = AlertDialog(
      scrollable: true,
      insetPadding: EdgeInsets.symmetric(horizontal: 10),
      title: Text(action + ' categoria.'),
      content: Container(
        child: Form(
          key: _editForm,
          child: Column(
            children: [
              TextFormField(
                autofocus: true,
                controller: _nombreCtrl,
                decoration: InputDecoration(labelText: 'Nombre.'),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Agrege un nombre.';
                  }
                  return null;
                },
              ),
              TextFormField(
                autofocus: true,
                controller: _nombreCtrl,
                decoration: InputDecoration(labelText: 'Categoria padre..'),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Agrege información a la tienda.';
                  }
                  return null;
                },
              ),
              TextFormField(
                minLines: 4,
                maxLines: 5,
                autofocus: true,
                controller: _descriptionCtrl,
                decoration: InputDecoration(labelText: 'Descripcion.'),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Agrege descripcion.';
                  }
                  return null;
                },
              )
            ],
          ),
        ),
      ),
      actions: [
        ElevatedButton(
            onPressed: () async {
              if (_editForm.currentState.validate()) {
                if (action == 'Crear') {
                  print('Crear');
                  categoryItem.categoryId = null;
                  categoryItem.name = _nombreCtrl.text;
                  categoryItem.description = _descriptionCtrl.text;
                  categoryItem.parentId = 0;
                  categoryItem.storeId = 3;
                  CategoryItem catNew =
                      await HttpCategoryService().createCat(categoryItem);
                  if (catNew == null) return print('err');
                } else {
                  print('Editar');
                }
                Navigator.pushNamed(context, 'categorias');
              }
            },
            child: Text('Aceptar')),
        TextButton(
            onPressed: () => Navigator.pop(context), child: Text('Cancelar'))
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
