import 'package:flutter/material.dart';

import '../const/const.dart' as localConst;

class MenuWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width * .8,
      child: Drawer(
        child: Container(
          color: Color(0xff1c313a),
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              Container(
                height: 130,
                child: DrawerHeader(
                    padding: EdgeInsets.only(left: 30),
                    child: Row(
                      children: [
                        CircleAvatar(
                            radius: 30,
                            backgroundColor: Colors.red[100],
                            backgroundImage: AssetImage(localConst.iconLogo)),
                        Container(
                            margin: EdgeInsets.only(left: 20),
                            child: Text(
                              localConst.textLogo,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 25),
                            ))
                      ],
                    )),
              ),
              for (var item in localConst.menuItems)
                ListTile(
                    leading: Icon(
                      item['icon'],
                      color: Colors.white,
                    ),
                    title: Text(
                      item['name'],
                      style: TextStyle(color: Colors.white, fontSize: 17),
                    ),
                    onTap: () => Navigator.pushNamed(context, item['route']))
            ],
          ),
        ),
      ),
    );
  }
}
