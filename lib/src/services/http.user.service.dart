import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

import '../models/user.model.dart';
import 'package:tienda/src/utils/http.utils.dart';

class HttpUserService {
  //
  Future<UserItem> loginWithUser({String username, String password}) async {
    var user = UserItem();
    var response = await http.post(
        Uri.http(HttpUtils().backendUrl, '/api/v1.0.0/users/get-token'),
        headers: HttpUtils().headers,
        body: {"username": username, "password": password});
    if (response.statusCode == 200) {
      var jsonResponde = convert.jsonDecode(response.body);
      var jsonData = jsonResponde['data'];
      user = UserItem.fromJson(jsonData);
      return user;
    } else {
      return null;
    }
  }
}

// Future<UserItem> loginWithToken({String token}) async {}
