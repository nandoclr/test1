import 'dart:convert' as convert;

import '../models/category.model.dart';
import 'package:http/http.dart' as http;
import '../utils/http.utils.dart';

class HttpCategoryService {
  //
  Future<List<CategoryItem>> getAllCats(String storeId) async {
    List<CategoryItem> categories = [];
    var response = await http.get(
        Uri.http(HttpUtils().backendUrl, '/api/monobusiness/categories',
            {'store_id': storeId}),
        headers: HttpUtils().headers);
    if (response.statusCode == 200) {
      var jsonResponde = convert.jsonDecode(response.body);
      List jsonData = jsonResponde['data'];
      categories = jsonData.map((item) => CategoryItem.fromJson(item)).toList();
      return categories;
    } else {
      return null;
    }
  }

  Future<CategoryItem> getSpecificCat(String categoryId) async {
    CategoryItem itemCat = new CategoryItem();
    var response = await http.get(
        Uri.http(HttpUtils().backendUrl,
            '/api/monobusiness/categories/' + categoryId),
        headers: HttpUtils().headers);
    if (response.statusCode == 200) {
      var jsonResponde = convert.jsonDecode(response.body);
      var jsonData = jsonResponde['data'];
      itemCat = CategoryItem.fromJson(jsonData);
      return itemCat;
    } else {
      return null;
    }
  }

  Future<CategoryItem> createCat(CategoryItem catItem) async {
    CategoryItem itemCat = new CategoryItem();
    var response = await http.post(
        Uri.http(HttpUtils().backendUrl, '/api/monobusiness/categories'),
        headers: HttpUtils().headers,
        body: catItem);
    if (response.statusCode == 200) {
      var jsonResponde = convert.jsonDecode(response.body);
      var jsonData = jsonResponde['data'];
      itemCat = CategoryItem.fromJson(jsonData);
      return itemCat;
    } else {
      return null;
    }
  }

  //Sin endpoint
  // Future<CategoryItem> updateCat(CategoryItem catItem) async {
  //   CategoryItem itemCat = new CategoryItem();
  //   var response = await http.put(
  //       Uri.http(localConst.backendUrl, '/api/monobusiness/categories'),
  //       headers: localConst.headers,
  //       body: catItem);
  //   if (response.statusCode == 200) {
  //     var jsonResponde = convert.jsonDecode(response.body);
  //     var jsonData = jsonResponde['data'];
  //     itemCat = CategoryItem.fromJson(jsonData);
  //     return itemCat;
  //   } else {
  //     return null;
  //   }
  // }

  Future<bool> removeCat(String catId) async {
    var response = await http.delete(
        Uri.http(
            HttpUtils().backendUrl, '/api/monobusiness/categories/' + catId),
        headers: HttpUtils().headers);
    if (response.statusCode == 200) {
      return false;
    } else {
      return true;
    }
  }
}
