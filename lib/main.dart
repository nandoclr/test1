import 'package:flutter/material.dart';

//Pages
import 'package:tienda/src/pages/categories.page.dart';
import 'package:tienda/src/pages/categoryInfo.page.dart';
import 'package:tienda/src/pages/home.page.dart';
import 'package:tienda/src/pages/login.page.dart';
import 'package:tienda/src/pages/products.page.dart';
import 'package:tienda/src/pages/puntoVenta.page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          primaryColor: Color(0xff027b76), accentColor: Color(0xffFF5722)),
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      initialRoute: 'home',
      routes: {
        'home': (BuildContext context) => HomePage(),
        'categorias': (BuildContext context) => CategoriasPage(),
        'productos': (BuildContext context) => ProductosPage(),
        'categoryInfo': (BuildContext context) => CategoryInfoPage(),
        'login': (BuildContext context) => LoginPage(),
        'venta': (BuildContext context) => PuntoVentaPga(),
      },
    );
  }
}
